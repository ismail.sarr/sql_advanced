--TD sql avancé

create table person(
    person_id            int not null,
    person_name                 char(25),
    health_status        varchar(50) not null,
    confirmed_status_date date not null,
    constraint k_person primary key (id_person)

);
create table place(
    place_id            int not null,
    place_name                 char(25),
    place_type                 char(25),

    constraint k_place primary key (place_id)

);

create table visit(
    visit_id            int not null,
    person_id            int not null,
    place_id            int not null,
    start_datetime      date not null,
    end_datetime        date not null,

    constraint k_visit primary key (visit_id),
    constraint fk_visit_person foreign key (person_id) references person (person_id),
    constraint fk_visit_place foreign key (place_id) references place (place_id)

);

drop table person;
drop table place;
drop table person;

--Insertion
--Requete

--Noms + statuts santé des personnes que Landyn Greer a croisé (même lieu, même moment)

select  p2.person_name, p2.health_status 
from  person p2 
inner join visit v
on v.person_id = p2.person_id 
where p2.person_id <>1 and place_id = 33 and
((start_datetime >= '2020-10-07 20:54:00.000' and start_datetime <= '2020-10-08 03:44:00.000') 
or (end_datetime >= '2020-10-07 20:54:00.000' and end_datetime <= '2020-10-08 03:44:00.000')
or (start_datetime <= '2020-10-07 20:54:00.000' and end_datetime >= '2020-10-08 03:44:00.000'))
or (p2.person_id <>1 and place_id = 88 and
((start_datetime >= '2020-09-26 18:28:00.000' and start_datetime <= '2020-09-26 21:40:00.000') 
or (end_datetime >= '2020-09-26 18:28:00.000' and end_datetime <= '2020-09-26 21:40:00.000')
or (start_datetime <= '2020-09-26 18:28:00.000' and end_datetime >= '2020-09-26 21:40:00.000')));

select  * from visit;
select  * from person;
select  * from place;


--Nombre de malades croisés dans un bar (même moment)

create table test as 
select p.person_id , v.visit_id , v.place_id ,p.health_status ,v.start_datetime,v.end_datetime, p.confirmed_status_date 
from person p inner join visit v 
on p.person_id = v.person_id;

select  * from test;

select count(t.person_id)
from test t
inner join place p
on t.place_id = p.place_id 
where  t.health_status = 'Sick'
and place_type='Bar'
and confirmed_status_date between start_datetime and end_datetime
and t.start_datetime between t.start_datetime and t.end_datetime;



--Noms des personnes que Taylor Luna a croisé

select * from visit 
where place_id in (72,8,84,26,5,84,29,90,4,52);

start_datetime between '2020-09-23 

